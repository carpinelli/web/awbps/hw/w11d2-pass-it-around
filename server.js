const express = require("express");


const app = express();
const PORT = 8082;


app.get("/", (request, response, next) =>
  {
    const bottleCount = 99;
    const nextBottleCount = bottleCount - 1;
    const html = `
      <div>
        <h1>${bottleCount} Bottles of beer on the wall!</h1>
        <h2>${bottleCount} Bottles of beer!</h2>
        <h2>
          <a href="/${nextBottleCount}">take one down, pass it around</a>
        </h2>
      </div>
    `;
    response.send(html);
  }
);
app.get("/:bottleCount", (request, response, next) =>
  {
    const bottleCount = Number(request.params.bottleCount);
    const nextBottleCount = bottleCount - 1;
    const noBeer = `
      <div>
        <h1>0 Bottles of beer on the wall!</h1>
        <h2>
          <a href="/">Begin Again!</a>
        </h2>
      `;
    const html = `
      <div>
        <h1>${bottleCount} Bottles of beer on the wall!</h1>
        <h2>${bottleCount} Bottles of beer!</h2>
        <h2>
          <a href="/${nextBottleCount}">take one down, pass it around</a>
        </h2>
      </div>
    `;
    
    bottleCount === 0 ? response.send(noBeer) : response.send(html);
  }
);


app.listen(PORT, () =>
  {
    console.log(`Listening on http://localhost:${PORT}`);
  }
);

